==========================
pdfposter
==========================

-------------------------------------------------------------
Scale and tile PDF images/pages to print on multiple pages.
-------------------------------------------------------------

:Author:    Hartmut Goebel <h.goebel@crazy-compilers.com>
:Version:   Version |VERSION|
:Copyright: 2008-2022 by Hartmut Goebel
:License:   GNU Public License v3 or later (GPL-3.0-or-later)
:Manual section: 1

.. raw:: manpage

   .\" disable justification (adjust text to left margin only)
   .ad l


SYNOPSIS
==========

``pdfposter`` <options> infile outfile

DESCRIPTION
============

.. include:: docs/_description.txt


OPTIONS
========

.. include:: docs/_options.txt

.. include:: docs/_box-definitions.txt


EXAMPLES
============

.. include:: docs/_examples1.txt

More examples including sample pictures can be found at
https://pdfposter.readthedocs.io/en/latest/Examples.html

.. include:: docs/_examples2.txt


SEE ALSO
=============

``poster``\(1),
``pdfnup``\(1) https://pypi.org/project/pdfnup/,
``pdfsplit``\(1) https://pypi.org/project/pdfsplit/,
``pdfgrid``\(1) https://pypi.org/project/pdfgrid/

Project Homepage https://pdfposter.readthedocs.io/

#
# Copyright 2008-2022 by Hartmut Goebel <h.goebel@crazy-compilers.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
[sdist]
formats=gztar

[bdist_wheel]
universal=1

[metadata]
name         = pdfposter
version      = attr: pdfposter.__version__
description  = Scale and tile PDF images/pages to print on multiple pages.
long_description = file: README.txt
long_description_content_type = text/x-rst
url          = https://pdfposter.readthedocs.io/
download_url = https://pypi.org/project/pdfposter/
author       = Hartmut Goebel
author_email = h.goebel@crazy-compilers.com
license      = GNU General Public License v3 or later (GPLv3+)
keywords     = pdf, poster
classifiers =
  Development Status :: 5 - Production/Stable
  Environment :: Console
  Intended Audience :: Developers
  Intended Audience :: End Users/Desktop
  Intended Audience :: System Administrators
  License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
  Natural Language :: English
  Operating System :: OS Independent
  Programming Language :: Python
  Programming Language :: Python :: 3
  Programming Language :: Python :: 3.6
  Programming Language :: Python :: 3.7
  Programming Language :: Python :: 3.8
  Programming Language :: Python :: 3.9
  Programming Language :: Python :: 3.10
  Topic :: Printing
  Topic :: Utilities

[options]
packages = pdfposter
include_package_data = True
zip_safe = True
install_requires =
  PyPDF2 >= 2.1.1, < 3

[options.entry_points]
console_scripts =
  pdfposter = pdfposter.cmd:run

[zest.releaser]
history-file = CHANGES
push-changes = no
tag-format = v{version}
tag-message = pdfposter {version}
tag-signing = yes
create-wheel = yes
python-file-with-version = pdfposter/__init__.py

[flake8]
ignore = E203,E225


# babel integration
[extract_messages]
# extract/update message template, for every update
input_dirs = pdfposter
output_file = pdfposter/locale/pdfposter.pot

[init_catalog]
# create a new translation from the template, once per language
domain = pdfposter
input_file = pdfposter/locale/pdfposter.pot
output_dir = pdfposter/locale

[update_catalog]
# update an existing translation from the template
domain = pdfposter
input_file = pdfposter/locale/pdfposter.pot
output_dir = pdfposter/locale

[compile_catalog]
# compile the message catalogs
domain = pdfposter
directory = pdfposter/locale
